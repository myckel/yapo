namespace :db do
  desc "Erase all tables"
    task clear: :environment do
      conn = ActiveRecord::Base.connection
      tables = conn.tables
      tables.each do |table|
        puts "Deleting #{table}"
        conn.drop_table(table, force: :cascade)
      end
    end

  desc "Seed production"
    task seed_production: :environment do
      load(File.join(Rails.root, "db", "seeds", "production.rb"))
    end
  desc "Seed development"
    task seed_development: :environment do
      load(File.join(Rails.root, "db", "seeds", "development.rb"))
    end 
  desc "Seed demo"
    task seed_demo: :environment do
      load(File.join(Rails.root, "db", "seeds", "demo.rb"))
    end    
end
