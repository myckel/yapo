# encoding: utf-8

# BASE SEED
if Rails.env == "development" || Rails.env == "test"
  load(File.join(Rails.root, "db", "seeds", "development.rb"))
elsif Rails.env == "production" || Rails.env == "staging"
  load(File.join(Rails.root, "db", "seeds", "production.rb"))
end

# SEEDS DEMO
load(File.join(Rails.root, "db", "seeds", "demo.rb"))

