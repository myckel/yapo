##  ¿Qué componentes usarías para securitizar tu API?
    * Rate limit en las anotaciones de ingress
    * Agregar un nivel de autenticación del servicio como JWT o similar.
    * Agregar layer https para que la conexión sea cifrada.
    * Variables de entorno
    * Correcto formato en consultas a la base de datos para evitar SQL Injection
    * Endpoints seguros, los controladores no deben exponer mas de lo necesario
## ¿Cómo asegurarías tu API desde el ciclo de vida de desarrollo?
	Para asegurar el ciclo de vida de desarrollo listare los elementos que a mi parecer son necesarios:

    * Una buena planificación donde se definan de manera correcta todos los requerimientos funcionales y no funcionales.
    * Para comenzar el desarrollo de la API  se debe contar con un plan sólido y estrategias que respondan al plan desarrollado.
    * Se deben realizar pruebas para garantizar qué se cumplan con las expectativas de rendimiento, funcionalidad y seguridad.
    * Una vez publicada la API, esta debe mantenerse para asegurar el correcto funcionamiento mediante actualizaciones para que no se vea comprometida la integridad de la API.
    * Trabajar con pruebas TDD para asegurar el 100% del coverage para generar un ciclo de integración continuo correcto
## ¿Qué podrías hacer para escalar horizontalmente la API?. Considera agregar nuevos componentes o el uso de un orquestador
    El diseño de la app debe stateless y nuestra persistencia debe estar cubierta por servicios stateful, para que el escalar se vuelva sencillo, para esto usaría un modelo basado en contenedores orquestados por Kubernetes. Si la solución cumple con el criterio de stateless para escalar solo se necesita aumentar el numero de replicas. En el caso que la solución no escale por temas de rendimiento es necesario destrabar los posibles cuellos de botella o escalar los componentes con problemas(ej. base de datos, storage, etc..)
## ¿Qué podrías hacer para escalar horizontalmente de manera automática?. Considera configuración (YAML para Kubernetes) o configuración de servicio PAAS
    Para realizar un auto escalado horizontal debemos crear un HorizontalPodAutoscaler, se debe considerar para la configuración del HPA el máximo uso de cpu antes de crear una nueva replica del pod.
