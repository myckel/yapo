$redis = Redis.new(url: ENV['REDIS_URL'], password: ENV['REDIS_PASSWORD'])

if defined?(Sidekiq)
  Sidekiq.configure_server do |config|
    config.on(:startup) do
      Sidekiq::Queue.new("scheduler").clear
      jobs = Sidekiq::ScheduledSet.new.
        select{|job| job.queue.in? ['scheduler'] }
      jobs.each(&:delete)
      GetUfJob.perform_now
    end
  end
end
