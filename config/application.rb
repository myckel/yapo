require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Yapo
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Config active jobs to use sidekiq
    config.active_job.queue_adapter = :sidekiq

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # Config generators
    config.generators do |g|
      #g.templates.unshift File::expand_path('../templates', __FILE__)
      g.orm            :active_record
      g.test_framework :rspec, fixture: true
    end

    # Configure Rake CORS
    config.middleware.insert_before 0, Rack::Cors, logger: (-> { Rails.logger }) do
      allow do
        origins '*'

        resource '*',
          headers: :any,
          methods: [:get, :post, :delete, :put, :patch, :options, :head],
          max_age: 0
      end
    end

    config.action_mailer.smtp_settings = {
      user_name: ENV['MANDRILL_USER_NAME'],
      password: ENV['MANDRILL_API_KEY'],
      address: "smtp.mandrillapp.com",
      port: 587,
      enable_starttls_auto: true,
      authentication: "login"
    }
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
  end
end
