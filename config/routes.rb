Rails.application.routes.draw do

  get :pi, to: 'pis#pi'
  get :uf, to: 'ufs#uf'

  # Kubernetes readinessProbe and livenessProbe
  get '/_health', to: 'application#health'
end
