class PisController < ApplicationController
  before_action :check_random_limit_length, only: [:pi]
  rescue_from ArgumentError, with: :argument_error

  # GET /pis
  def pi
    if params[:random_limit]
      result = PiService.new(params[:random_limit]).get
      render json: { PiCalc: result }
    else
      render json: { error: I18n.t('.missing_random_limit') }, status: :bad_request
    end
  end

  private
  # Only allow a list of trusted parameters through.
  def pi_params
    params.fetch()
  end

  # Check random limit to avoid app hang out
  def check_random_limit_length
    render json: {
        error: I18n.t('.random_limit_lenght_error')
        }, status: :bad_request if params[:random_limit].to_i > 100000
  end

  protected

  def argument_error(exception)
    render json: { error: exception.message }, status: :bad_request
  end
end
