class UfsController < ApplicationController
  # GET /uf
  def uf
    result = UfService.new.today
    render json: result
  end
end
