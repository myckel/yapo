class ApplicationController < ActionController::API

  # Kubernetes readinessProbe and livenessProbe
  def health
    render json: { "success": "true" }
  end
end
