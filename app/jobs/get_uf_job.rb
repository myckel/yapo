require 'sidekiq-scheduler'
class GetUfJob < ApplicationJob
  queue_as :scheduler

  def perform
    UfService.new.today
  end
end
