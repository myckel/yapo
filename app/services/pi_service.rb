require "bigdecimal/math"
class PiService
  attr_accessor :random, :decimals, :random_limit

  def initialize(random_limit=nil)
    @random_limit = random_limit

    validate!

    @random_limit = @random_limit.to_i
    from = @random_limit / 2
    to = @random_limit + 1
    @random = rand(from..to)
    @decimals = @random + 1
  end

  # Return the number PI with the random number of decimals calculated
  def get
    pi = BigMath.PI(decimals).to_s[0..decimals]
    pi = pi.gsub('.','') if random == 0
    pi
  end

  private
  # check if it is a number
  def is_number?
    true if Float(random_limit) rescue false
  end

  # validation
  def validate!
    raise ArgumentError.new(
      I18n.t('.random_limit_type_argument_error')) unless is_number?
    raise ArgumentError.new(I18n.t('.random_limit_argument_error')) if random_limit.to_i.negative?
  end
end
