require "bigdecimal/math"
class UfService

  def initialize
  end

  # Get the current UF value from redis
  def today
    uf = $redis.get(:uf)
    if uf
      uf = JSON.parse(uf)
      serie = uf.dig('serie')&.first
      {
        uf: {
          value: serie.dig('valor').to_s,
          date: serie.dig('fecha')
        }
      }
    else
      uf = get_uf
      $redis.set(:uf, uf.to_json)
      serie = uf.dig('serie')&.first
      {
        uf: {
          value: serie.dig('valor').to_s,
          date: serie.dig('fecha')
        }
      }
    end
  end


  private
  # Call to mindicador.cl to get today UF
  def get_uf
    response = Faraday.get("https://mindicador.cl/api/uf/#{DateTime.now.strftime('%d-%m-%Y')}")
    if response.status == 200
      uf = JSON.parse(response.body)
      if uf.blank?
        GetUfJob.perform_async
      else
        $redis.set(:uf, uf.to_json)
      end
    end
    uf
  end
end
