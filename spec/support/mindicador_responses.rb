module MindicadorResponse
  def uf_response
    Struct.new(:status, :body).new(200,
      {
        "version": "1.7.0",
        "autor": "mindicador.cl",
        "codigo": "uf",
        "nombre": "Unidad de fomento (UF)",
        "unidad_medida": "Pesos",
        "serie": [
            {
                "fecha": DateTime.now.strftime('%Y-%m-%dT00:00:00.000z'),
                "valor": '31260.83'
            }
        ]
      }.to_json
    )
  end
end
