# frozen_string_literal: true

require_relative 'helpers/response_helpers'

RSpec.configure do |config|
  config.include Support::ResponseHelpers, type: :request
end
