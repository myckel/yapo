require 'rails_helper'

RSpec.describe PiService, type: :model do
  describe '#get' do
    let(:random_limit){ rand(1..10) }
    context 'correct response' do
      it 'should response PI equal to 3.14159' do
        allow_any_instance_of(PiService).to receive(:rand).and_return(5)
        response = PiService.new(random_limit).get
        expect(response).to eq('3.14159')
      end
      it 'should response PI equal to 3.1415926535' do
        allow_any_instance_of(PiService).to receive(:rand).and_return(10)
        response = PiService.new(random_limit).get
        expect(response).to eq('3.1415926535')
      end
      it 'should response PI equal to 3.141592653' do
        allow_any_instance_of(PiService).to receive(:rand).and_return(9)
        response = PiService.new(random_limit).get
        expect(response).to eq('3.141592653')
      end
    end
  end
end