require 'rails_helper'
include MindicadorResponse

RSpec.describe UfService, type: :model do
  describe '#today' do
    context 'correct response' do
      it 'should response today UF' do
        allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(uf_response)
        response = UfService.new.today
        expect(response[:uf][:value]).to eq(
          JSON.parse(uf_response.body).dig('serie').first.dig('valor')
        )
      end
    end
  end
end