# YAPO API TEST

This API is for testing prouporses. With this API you will be able to:

* Get the PI number with random decimals based on the parameter random_limit
* Get the current value of the UF

The Kubernetes orchestration was made thinking to be deployed on GCP, with CircleCi as a CD/CI.
All the files and variables are just an example of the correct configuration of the deployment, NGNIX Ingress and cert. manager.

## Extras

Extras folder contains the answer for the questions, the diagrams, and the postman collection to test the API.

## Prerequisites

```
Docker 4.4.2
Make 3.81
Yarn 1.22.17
Node 16.14.0
Artillery 2.0.0-11
```

## App Setup

Run the following commands on the root directory of the app.

Image Build:
```
make build
```

Database creation:
```
make create_db
```

Running the container:
```
make run
```


## Testing

Run the following command

Rspec:
```
make rspec
```
## Endpoints

Get the current value of the UF.
```
GET localhost/uf
```
Get the PI number with a random limit of decimals where {{random_limit_number}} is the limit for the random number of decimals.
```
GET localhost/pi?random_limit={{random_limit_number}}
```
## Artillery

The following command will run the yaml to test the concurrence of the API, the output will be a JSON with the test results.
```
artillery run artillery/config.yml --output artillery/test.json
```
Generate the report
```
artillery report --output artillery/report.html artillery/test.json
```
Open the HTML file on artillery/report.html to see the results.

## Additional commands

Enter to the container:
```
make enter
```
Migrations:
```
make migrate
```
