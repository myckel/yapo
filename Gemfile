source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.9'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0', '>= 6.0.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Simple, Heroku-friendly Rails app configuration using ENV and a single YAML file
gem "figaro"

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# ActiveModel::Serializers allows you to generate your JSON in an object-oriented and convention-driven manner.
gem 'active_model_serializers', '~> 0.10.2'

# A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
gem 'kaminari'

# Easy API pagination for Rails
gem 'pager_api'

# The ultimate pagination ruby gem
gem 'pagy'

# Simple, efficient background processing for Ruby
gem 'sidekiq', '>= 6.4.1'

# Lightweight job scheduler extension for Sidekiq
gem 'sidekiq-scheduler'

# Simple, but flexible HTTP client library, with support for multiple backends.
gem 'faraday'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # rspec-rails is a testing framework for Rails 3.x, 4.x and 5.x.
  gem 'rspec-rails', '~> 3.7'
  # A library for setting up Rails objects as test data
  gem 'factory_bot_rails'
  # Makes tests easy on the fingers and the eyes
  gem 'shoulda', '~> 3.6'
  # Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing.
  gem 'database_cleaner', '~> 1.5'


end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Replaces the standard Rails error page with a much better and more useful error page
  gem "better_errors"
  gem "binding_of_caller"

  # A library for generating fake data such as names, addresses, and phone numbers.
  gem 'faker', '~> 2.8', '>= 2.8.1'
  # Replace rails console with pry
  gem 'pry'
  gem 'pry-rails'
  # Preview mail in the browser instead of sending.
  gem 'letter_opener'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
