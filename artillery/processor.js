
function generateLimitNumber(requestParams, ctx, ee, next) {
  ctx.vars["random_limit"] = Math.floor(Math.random() * (10000 - 0) + 0)

  return next();
}

module.exports = {
  generateLimitNumber,
};