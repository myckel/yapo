# FROM phusion/passenger-customizable
FROM phusion/passenger-ruby26

# Set correct environment variables
ENV HOME /root
ENV APP_HOME /home/app/webapp

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Ruby 2.5.7 set as default
RUN bash -lc 'rvm --default use ruby-2.6.9'

# Clean Sources
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /etc/apt/sources.list.d/*

RUN apt-get update -qq

# Install apt dependencies
RUN apt-get install -y \
  curl \
  wget \
  chrpath \
  libssl-dev \
  libxml2-dev \
  libxft-dev \
  libfreetype6 \
  libfreetype6-dev \
  libfontconfig1 \
  libfontconfig1-dev \
  libmagickwand-dev\
  libmagickcore-dev\
  libcurl4-openssl-dev \
  imagemagick\
  git \
  unzip \
  zlib1g-dev \
  libxslt-dev \
  sqlite3 \
  imagemagick \
  postgresql-client \
  unzip \
  openjdk-8-jre-headless \
  xvfb \
  libxi6 \
  libgconf-2-4

# Installing tzdata
# If you do not run the following error when accessing the Rails application
# tzinfo-data is not present.
# Please add gem 'tzinfo-data' to your Gemfile
# and run bundle install (TZInfo :: DataSourceNotFound)
RUN apt-get install -y tzdata

# tcl package
RUN apt-get install -y tcl

# Workdir for bundle and bower
WORKDIR /home/app/webapp/

# Add files
ADD . /home/app/webapp/

# Installing gems
RUN bundle install

# Change /home/app/webapp owner to user app
RUN chown -R app:app /home/app/webapp/

# Enable ssh and insecure key permanently (development)
RUN rm -f /etc/service/sshd/down
RUN /usr/sbin/enable_insecure_key

# Add init scripts
ADD docker/my_init.d/*.sh /etc/my_init.d/

# Ensure premissions to execute and Unix newlines
RUN chmod +x /etc/my_init.d/*.sh && sed -i 's/\r$//' /etc/my_init.d/*.sh

# Ensure permission to execute and Unix newlines on bin files
RUN chmod +x /home/app/webapp/bin/* && sed -i 's/\r$//' /home/app/webapp/bin/*

# Clean up APT when done
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*

# add alias for rails console
RUN cd && echo "alias rc='bundle exec rails c'" >>  .bashrc
RUN cd && echo "alias rs='rails s -b 0.0.0.0'" >>  .bashrc
